# A different school story

Short mobile point and click adventure with illustrations and story by [Kürsat Salim](CREDITS.md)

![](screenshots/01.png)

![](screenshots/02.png)

## Eine außergewöhnliche Schulgeschichte <kbd>de</kbd>

In dieser Geschichte bist du kein Leser, nein du tauchst in eine Welt voller Magie und mystischen Zauberkräften ein.
Erlebe hautnah wie es sich anfühlt die Hauptfigur in einer faszinierenden Welt zu sein, denn die Entscheidung wohin du gehst und mit wem du sprichst, liegt in deiner Hand. Erforsche spannende Einrichtungen. Rede mit einzigartigen Charakteren und gehe auf eine nervenzerreibende Spurensuche einer mysteriösen Explosion.
