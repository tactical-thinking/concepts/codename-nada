--[[
	# Story - German / DE

	## Information for translators

	Only the fields `char_name` and `text` should be translated, e.g.:
			
	```
	...
	convo = {{
		char_name = "MC",
		text = "Hmmm... von wem der Schülerausweis wohl sein könnte. Ich kann den Namen leider nicht erkennen. Vielleicht kann mir Herr Schmitt weiterhelfen."
	}}
	...
	```

	becomes

	```
	...
	convo = {{
		char_name = "MC",
		text = "Hmmm... Who could be the owner of this student ID? I can't figure out what it says. Maybe Mr. Schmitt can help me out."
	}}
	...
	```
]]

return
{
	-- scene 1
	-- asking maks about the explosion (wuluu gives a hint to go on the roof)
	[1] = {
		{
			main_convo = true,
			char = hash("/maks"),
			convo = {
				{
					char_name = "MC",
					text = "Hey, hallo, ich habe da eine kleine Frage an dich. Kannst du mir weiterhelfen?"
				},
				{
					char_name = "Maks",
					text = "Na klar, rück raus. Was liegt dir am Herzen?"
				},
				{
					char_name = "MC",
					text = "Am Vormittag gab es doch eine Explosion, weißt du von wo die herkam?"
				},
				{
					char_name = "Maks",
					text = "Nicht ganz, aber ich kann mit Gewissheit sagen, dass sie von der anderen Seite der Schule kam. Höchstwahrscheinlich aus der 3A."
				},
				{
					char_name = "MC",
					text = "Danke, du hast mir weitergeholfen."
				},
				{
					char_name = "Maks",
					text = "Immer wieder gerne."
				},
			}
		},
		{
			char = hash("/mc"),
			convo = {{
				char_name = "MC",
				text = "Was zur Hölle ist hier passiert? Besser ich gehe schnell weg von hier."
			}}
		},
		{
			char = hash("/hiko"),
			convo = {{
				char_name = "Hiko",
				text = "Ich frage mich, warum du noch hier bist."
			}}
		},
		{
			char = hash("/markus"),
			convo = {{
				char_name = "Markus",
				text = "Heute war kein leichter Tag. So etwas sieht man nicht alle Tage."
			}}
		},
		{
			char = hash("/wuulu"),
			convo = {{
				char_name = "Wuulu",
				text = "Ich frage mich von wo diese Explosion kam? Vielleicht hat man sie ja vom Schuldach aus gesehen."
			}}
		},
		{
			char = hash("/peta"),
			convo = {{
				char_name = "Peta",
				text = "Wenn du etwas nicht zu Ende bringst, darfst du auch nicht damit aufhören."
			}}
		},
		{
			char = hash("/sementa"),
			convo = {{
				char_name = "Sementa",
				text = "Es gibt da so einen Jungen, den ich mag und ... wer bist du überhaupt? Verschwinde!"
			}}
		},
		{
			char = hash("/hive"),
			convo = {{
				char_name = "Hive",
				text = "Was willst du!? Ich hatte einen schlechten Tag, also verschwinde!"
			}}
		},
		{
			char = hash("/schmitt"),
			convo = {{
				char_name = "Herr Schmitt",
				text = "Was ist los? Falls du nichts brauchst, gehe ich wieder zurück an die Arbeit."
			}}
		},
		{
			char = hash("/hige"),
			convo = {{
				char_name = "Hige",
				text = "Wenn du mich nicht kennst, dann klopf auch nicht an meiner Tür."
			}}
		}
	},
	-- scene 2
	-- monologue after finding the student id
	[2] = {
		{
			char = hash("/maks"),
			convo = {{
				char_name = "MC",
				text = "Ob ich die Explosion gesehen habe? Nicht ganz, aber sie kam von der anderen Seite der Schule. Höchstwahrscheinlich aus der 3A."
			}}
		},
		{
			main_convo = true,
			char = hash("/mc"),
			convo = {{
				char_name = "MC",
				text = "Hmmm... von wem der Schülerausweis wohl sein könnte. Ich kann den Namen leider nicht erkennen. Vielleicht kann mir Herr Schmitt weiterhelfen."
			}}
		}
	},
	-- scene 3
	-- asking teacher Schmitt about the student ID
	[3] = {
		{
			main_convo = false,
			char = hash("/mc"),
			convo = {{
				char_name = "MC",
				text = "Vielleicht kann mir Herr Schmitt weiterhelfen."
			}}
		},
		{
			main_convo = true,
			char = hash("/schmitt"),
			convo = {
				-- schmitts door opens
				{
					char_name = "Herr Schmitt",
					text = "Was ist los?"
				},
				{
					char_name = "MC",
					text = "Es tut mir leid, dass ich Sie stören muss, aber kennen Sie den Besitzer von diesem Schülerausweis?"
				},
				{
					char_name = "Herr Schmitt",
					text = "Ein Schülerausweis? Ich kann leider nicht erkennen, wem er gehört. Wo hast du ihn denn gefunden?"
				},
				{
					char_name = "MC",
					text = "In der 3A auf dem Boden."
				},
				{
					char_name = "Herr Schmitt",
					text = "In der 3A sagst du. Hmmm, frag doch mal Markus. Vielleicht weiß er ja wem der Schülerausweis gehört. Er ist bestimmt hier irgendwo im 2. Stock."
				},
				{
					char_name = "MC",
					text = "Danke, das mache ich"
				}
			}
		}
	},
	-- scene 4
	-- asking Markus about the student ID
	[4] = {
		{
			char = hash("/schmitt"),
			convo = {{
				char_name = "Herr Schmitt",
				text = "So wie ich Markus kenne, geistert er irgendwo im 2. Stock herum."
			}}
		},
		{
			main_convo = true,
			char = hash("/markus"),
			convo = {
				{
					char_name = "Markus",
					text = "... Heute war kein leichter Tag. So etwas sieht man nicht alle Tage."
				},
				{
					char_name = "Markus",
					text = "Hmmm, was ist das?"
				},
				{
					char_name = "MC",
					text = "Bist du Markus?"
				},
				{
					char_name = "Markus",
					text = "Der bin ich mit Leib und Seele, brauchst du etwas Bestimmtes?"
				},
				{
					char_name = "MC",
					text = "Ja, ich habe diesen Schülerausweis in der 3A gefunden und wollte wissen, ob du seinen Besitzer kennst."
				},
				{
					char_name = "Markus",
					text = "Lass mal sehen..."
				},
				{
					char_name = "Markus",
					text = "Tut mir leid, aber ich kann den Typen leider auch nicht erkennen."
				},
				{
					char_name = "Markus",
					text = "Du meintest doch, du hast ihn in der 3A gefunden, stimmts? Es kann gut sein, dass er einem der zwei Schüler gehört, die heute Vormittag gekämpft haben."
				},
				{
					char_name = "Markus",
					text = "Einer von ihnen heißt Hive und an den anderen kann ich mich nicht erinnern. Bring den Ausweis doch einfach zu ihm. Vielleicht ist es ja seiner. Ich habe ihn vor kurzem in seiner Klasse gesehen schau doch mal nach."
				},
				{
					char_name = "MC",
					text = "Danke, du hast mir wirklich weitergeholfen."
				}
			}
		}
	},
	-- scene 5
	-- talking to hive
	[5] = {
		{
			char = hash("/markus"),
			convo = {{
				char_name = "Markus",
				text = "Hive ist vermutlich in der 3A."
			}}
		},
		{
			main_convo = true,
			char = hash("/hive"),
			convo = {
				{
					char_name = "Hive",
					text = "Was willst du!? Ich hatte einen schlechten Tag. Also verschwinde!"
				},
				{
					char_name = "MC",
					text = "Das hier gehört vermutlich dir."
				},
				{
					char_name = "Hive",
					text = "Was ist das? Das könnte mein Schülerausweis sein. Danke, und entschuldige bitte meine grobe Art. Ich hatte heute nun mal keinen guten Tag."
				},
				{
					char_name = "MC",
					text = "Warum das denn?"
				},
				{
					char_name = "Hive",
					text = "Ach, nur ein kleiner Streit unter Geschwistern. Mach dir keinen Kopf."
				},
				{
					char_name = "MC",
					text = "Gut, wenn du das sagst."
				},
			}
		}
	},
	-- scene 6
	-- monologue after leavin class 3A
	[6] = {
		{
			char = hash("/hive"),
			convo = {{
				char_name = "Hive",
				text = "Wie schon gesagt, es war nur eine kleine Auseinandersetzung zwischen zwei Geschwistern. Zerbrich dir nicht weiter den Kopf darüber."
			}}
		},
		{
			main_convo = true,
			char = hash("/mc"),
			convo = {{
				char_name = "MC",
				text = "Ich sollte dem Lehrer Bescheid geben, dass ich den Besitzer des Ausweises gefunden habe."
			}}
		}
	},
	-- scene 7
	-- talking to teacher again
	[7] = {
		{
			main_convo = true,
			char = hash("/schmitt"),
			convo = {
				{
					char_name = "Herr Schmitt",
					text = "Du hast den Schülerausweis seinem Besitzer gegeben? Gut, danke."
				},
				{
					char_name = "MC",
					text = "Kein Problem."
				},
				{
					char_name = "MC",
					text = "Ach ja, noch etwas... Kennen Sie die Geschwister von Hive?"
				},
				{
					char_name = "Herr Schmitt",
					text = "Lass mich überlegen... Soweit ich weiß, hat Hive eine kleine Schwester, die Hiko heißt. Sie geht in die 2. Klasse. Mehr weiß ich leider auch nicht."
				}
			}
		}
	},
	-- scene 8
	-- talking to hiko (hives sister)
	[8] = {
		{
			char = hash("/schmitt"),
			convo = {
				char_name = "Herr Schmitt",
				text = "Soweit ich weiß, hat Hive eine kleine Schwester, die Hiko heißt. Sie geht in die 2. Klasse. Mehr weiß ich leider auch nicht."
			}
		},
		{
			main_convo = true,
			char = hash("/hiko"),
			convo = {
				{
					char_name = "Hiko",
					text = "Ich frage mich, warum du noch hier bist."
				},
				{
					char_name = "MC",
					text = "Ich habe dich gesucht und muss dir eine wichtige Frage stellen... Obwohl, jetzt wo ich hier bin, bin ich mir bei der Frage auch nicht mehr sicher."
				},
				{
					char_name = "Hiko",
					text = "Eine wichtige Frage? Jetzt werde ich nervös. Was möchtest du mich denn fragen?"
				},
				{
					char_name = "MC",
					text = "Es geht um deinen Bruder Hive."
				},
				{
					char_name = "Hiko",
					text = "Ach so, natürlich, schieß los."
				},
				{
					char_name = "MC",
					text = "Hast du dich heute mit Hive geprügelt?"
				},
				{
					char_name = "Hiko",
					text = "Ich? Geprügelt? Mit meinem großen Bruder? Sei doch bitte nicht albern. Wer hat dir das erzählt?"
				},
				{
					char_name = "MC",
					text = "Ich habe nur gehört, dass sich Hive mit einem seiner Geschwister geprügelt hat, und das bist, so wie es aussieht, nicht du."
				},
				{
					char_name = "Hiko",
					text = "Ich weiß nicht, wovon du redest, aber ich habe meinen Bruder Hive heute noch kein einziges Mal gesehen. Ich habe aber auch noch einen älteren Bruder, Hige. Am besten du fragst ihn. Er ist vermutlich nach Hause ins Wohnheim West gegangen."
				},
				{
					char_name = "Hiko",
					text = "Tür  Nummer 203, wenn du es genau wissen willst."
				},
				{
					char_name = "MC",
					text = "Danke, du bist mir eine große Hilfe."
				},
				{
					char_name = "Hiko",
					text = "Aber nicht doch."
				}
			}
		}
	},
	-- scene 9
	-- talking to hige (hives bro)
	[9] = {
		{
			char = hash("/hiko"),
			convo = {{
				char_name = "Hive",
				text = "Mein Bruder Hige ist im Wohnheim West, Tür 203, wenn du mit ihm reden möchtest."
			}}
		},
		{
			main_convo = true,
			char = hash("/hige"),
			convo = {
				-- higes door opens
				{
					char_name = "Hige",
					text = "Du kennst mich nicht, also klopf auch nicht an meiner Tür."
				},
				{
					char_name = "MC",
					text = "Du bist doch Hige, nicht wahr?"
				},
				{
					char_name = "Hige",
					text = "Woher kennst du meinen Namen?"
				},
				{
					char_name = "MC",
					text = "Ich habe mit deiner Schwester geredet und sie meinte, ich soll dich befragen."
				},
				{
					char_name = "Hige",
					text = "Befragen!? Wegen was?"
				},
				{
					char_name = "MC",
					text = "Du bist doch für die Explosion am Vormittag verantwortlich, stimmts?  Was war der Grund dafür, dass du deinen Bruder angegriffen hast?"
				},
				{
					char_name = "Hige",
					text = "Dieser Typ hat es nicht anders verdient. Andauernd behandelt er Mädchen als wären sie wertlose Gegenstände und dieses eine mal ist er nun allemal zu weit gegangen!"
				},
				{
					char_name = "MC",
					text = "Ich verstehe. Das ist durchaus ein Grund um wütend zu werden. Wenn du nichts dagegen hast, würde ich der Sache auf den Grund gehen und deinen Bruder Hive zu dieser Sache befragen."
				},
				{
					char_name = "Hige",
					text = "Mach doch was du willst."
				}
			}
		}
	},
	-- scene 10
	-- talking to hive again
	[10] = {
		{
			char = hash("/hige"),
			convo = {{
				char_name = "Hige",
				text = "Ich dachte du wolltest unbedingt mit meinem Bruder reden, nicht das es mich kümmert."
			}}
		},
		{
			main_convo = true,
			char = hash("/hive"),
			convo = {
				{
					char_name = "Hive",
					text = "Du schon wieder? Kann ich dir behilflich sein?"
				},
				{
					char_name = "MC",
					text = "Ich habe da eine Frage zu heute Vormittag."
				},
				{
					char_name = "Hive",
					text = "Eigentlich würde ich dir für diese Aufdringlichkeit auf die Schnauze hauen."
				},
				{
					char_name = "Hive",
					text = "Aber schieß los, du hast mir immerhin meinen Schülerausweis wiedergebracht."
				},
				{
					char_name = "MC",
					text = "Hige meint, du hast ein Mädchen belästigt?"
				},
				{
					char_name = "Hive",
					text = "Was soll das!? Es stimmt zwar, dass ich, mit Ausnahme meiner kleinen Schwester, nicht viel von Mädchen halte, aber ich würde so etwas nie tun."
				},
				{
					char_name = "MC",
					text = "Kannst du mir dann sagen, wieso er so etwas behauptet?"
				},
				{
					char_name = "Hive",
					text = "Willst du mich eigentlich auf den Arm nehmen? Woher soll ich das wissen?"
				},
				{
					char_name = "Hive",
					text = "Obwohl, warte mal... Es gibt da ein Mädchen, das die ganze Zeit vor seiner Klasse steht. Ich habe da so eine Beführchtung, dass sie damit etwas zu tun haben könnte."
				},
				{
					char_name = "Hive",
					text = "Diese Stalkerin ist vermutlich jetzt auch auf der anderen Seite der Schule und wartet aus irgendeinem Grund vor der Klasse meines Bruders."
				},
				{
					char_name = "MC",
					text = "Menschen gibts."
				},
				{
					char_name = "MC",
					text = "Also gut, dann verlasse ich mich mal auf das, was du gesagt hast."
				},
				{
					char_name = "MC",
					text = "Eine Frage hätte ich aber noch."
				},
				{
					char_name = "Hive",
					text = "Die wäre?"
				},
				{
					char_name = "MC",
					text = "Wie kann es sein, dass du keinen Kratzer abbekommen hast?"
				},
				{
					char_name = "Hive",
					text = "Nun ja, weißt du, ich habe die Fähigkeit mich in Feuer zu verwandeln, aber das bedeutet nicht, dass ich ohne Kratzer da rausgekommen bin."
				},
				-- _`hive`s sprite changes to `special`_
				{
					char_name = "$spritechange_hive",
					text = "special"
				},
				{
					char_name = "Hive",
					text = "Den ganzen Schaden hat mein Feuerkörper einstecken müssen, deswegen stehe ich jetzt hier „ohne Kratzer“."
				},
				-- _`hive`s sprite changes to `default`_
				{
					char_name = "$spritechange_hive",
					text = "default"
				},
				{
					char_name = "MC",
					text = "Ich verstehe, dann mach ich mich auf den Weg. Bis bald."
				}
			}
		},
		{
			char = hash("/peta"),
			convo = {{
				char_name = "Peta",
				text = "Wenn man das Unmögliche ausschließt, bleibt nur mehr eine einzige Wahrheit übrig."
			}}
		}
	},
	-- scene 11
	-- talking to sementa ("stalker")
	[11] = {
		{
			char = hash("/hive"),
			convo = {{
				char_name = "Hive",
				text = "Du bist immer noch hier? Das Mädchen sollte auf der anderen Seite der Schule sein."
			}}
		},
		{
			main_convo = true,
			char = hash("/sementa"),
			convo = {
				{
					char_name = "Sementa",
					text = "... Es gibt da so einen Jungen, den ich mag und ... wer bist du überhaupt? Verschwinde!"
				},
				{
					char_name = "MC",
					text = "Meinst du Hige? Er schaut echt gut aus und scheint ganz nett zu sein."
				},
				{
					char_name = "Sementa",
					text = "Ja, und er ist immer so aufmerksam und großzügig. Aber was willst du?"
				},
				{
					char_name = "MC",
					text = "Ich wollte nur wissen, ob du diejenige bist, die ich suche. Weißt du eigentlich was heute Nachmittag mit Hige passiert ist?"
				},
				{
					char_name = "Sementa",
					text = "Nein. Ihm ist doch wohl nichts Schlimmes zugestoßen, oder?"
				},
				{
					char_name = "MC",
					text = "Nun ja, er hatte heute eine schwere Auseinandersetzung mit seinem Bruder und dieser hat nun schwere Verletzungen."
				},
				{
					char_name = "Sementa",
					text = "Was sagst du da? Hige hat seinen eigenen Bruder schwer verletzt... und das nur weil ich... Aber..."
				},
				{
					char_name = "MC",
					text = "Also steckst du hinter dem Streit zwischen den beiden Brüdern?"
				},
				{
					char_name = "Sementa",
					text = "Aber das war doch nicht meine Absicht. Ich... ich wollte doch nur, dass er mich bemerkt und mit mir Zeit verbringt."
				},
				{
					char_name = "MC",
					text = "Und deswegen hast du Hige eine Lüge erzählt damit er sich um dich kümmert? Aufgrund deiner egoistischen Art hätte das Hive fast sein Leben gekostet..."
				},
				{
					char_name = "Sementa",
					text = "Oh mein Gott, was habe ich nur getan... Ich muss das wieder in Ordnung bringen. Bitte gehe zu Hige und frage ihn, ob er nicht mit mir reden möchte. Ich bitte dich darum."
				}
			}
		}
	},
	-- scene 12
	-- explaining the situation to hige
	[12] = {
		{
			char = hash("/sementa"),
			convo = {{
				char_name = "Sementa",
				text = "Sprich mit Hige. Ich bitte dich."
			}}
		},
		{
			main_convo = true,
			char = hash("/hige"),
			convo = {
				{
					char_name = "Hige",
					text = "Ach, du bist es. Hast du alle deine Antworten bekommen?"
				},
				{
					char_name = "MC",
					text = "Kann man so sagen, aber hör mir mal zu. Sementa, die dir das über Hive erzählt hat, hat gelogen. In Wirklichkeit ist Hive unschuldig und sie wollte nur deine Aufmerksamkeit."
				},
				{
					char_name = "Hige",
					text = "Stimmt das auch wirklich? Du lügst mich garantiert nicht an?"
				},
				{
					char_name = "MC",
					text = "Nein, aber sie möchte sich mit dir treffen und alles in Ordnung bringen."
				},
				{
					char_name = "Hige",
					text = "Wenn das stimmt, was du mir erzählst, dann hat diese Sementa ein echtes Problem."
				},
				{
					char_name = "Hige",
					text = "Aber gut, um alles zu klären werde ich mich wohl mit ihr treffen müssen."
				}				
			}
		}
	},
	-- scene 13
	-- epilogue / secret
	[13] = {
		{
			main_convo = true,
			char = hash("/epilogue"),
			convo = {
				{
					char_name = "Epilog",
					text = "Hige und Sementa treffen sich und Sementa tut es unendlich Leid, dass sie Hige angelogen hat. Daraufhin treffen sich die beiden mit Hive und erklären die ganze Sache. Hive spricht daraufhin nicht mehr mit seinem Bruder. Nach 2 Monaten ist jedoch alles wieder beim Alten."
				},
				{
					char_name = "Credits",
					text = "Story & Illustrations: Kürsat Salim\nDevelopment: Lukas Kasticky\n\n"
				},
				{
					char_name = "Credits",
					text = "Music: Eric Matyas\nFonts: Font Diner\n\n"
				},
				{
					char_name = "Credits",
					text = "Libraries: Brian Kramer, Björn Ritzl, Ross Grams, Lukas Kasticky\n\n"
				},
				{
					char_name = "Credits",
					text = "See www.tactical.cf for more information"
				}
			}
		},
		{
			char = hash("/hiko"),
			convo = {{
				char_name = "Hiko",
				text = "Ach du bist es. Danke nochmal für damals."
			}}
		},
		{
			char = hash("/markus"),
			convo = {{
				char_name = "Markus",
				text = "Langweilig ist doch immer am besten."
			}}
		},
		{
			char = hash("/wuulu"),
			convo = {{
				char_name = "Wuulu",
				text = "Hast du noch etwas vor?"
			}}
		},
		{
			char = hash("/sementa"),
			convo = {{
				char_name = "Sementa",
				text = "Danke nochmal für damals. Jetzt verstehe ich mich mit Hige besser denn je zuvor."
			}}
		},
		{
			char = hash("/hive"),
			convo = {{
				char_name = "Hive",
				text = "Hey, ist alles in Ordnung? Sag bescheid, wenn du meine Hilfe brauchst."
			}}
		},
		{
			char = hash("/schmitt"),
			convo = {{
				char_name = "Herr Schmitt",
				text = "Bleibe nicht zu lange in der Schule, ja? "
			}}
		},
		{
			char = hash("/hige"),
			convo = {{
				char_name = "Hige",
				text = "Was für ein ungewöhnlicher Besuch. Melde dich einfach, wenn du etwas brauchst."
			}}
		},
		{
			char = hash("/maks"),
			convo = {{
				char_name = "Maks",
				text = "Was für ein schöner Tag um etwas frische Luft zu schnappen."
			}}
		},
		{
			char = hash("/peta"),
			convo = {
				{
					char_name = "Peta",
					text = "Es ist nicht wichtig wer du warst. Wichtig ist nur wer du sein möchtest und dass du darauf hinarbeitest."
				},
				{
					char_name = "Peta",
					text = "Oh, und hast du gewusst, dass ich den ersten Preis bei der weltweiten Matheolympiade gewonnen habe?"
				},
				{
					char_name = "Peta",
					text = "Ich bin auch Landesmeister in Schach."
				},
				{
					char_name = "Peta",
					text = "Aber das ist noch nicht die Spitze des Eisberges."
				}
			}
		}
	},
	-- after epilogue
	[14] = {

	}
}
