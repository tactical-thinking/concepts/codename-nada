local sdata = require "main.story.storydata_de"

-- module data
local mdata = {
	scene = 1
}

-- module
local M = {
	
}

-- return conversation with character
function M.get_convo(character, scene)

	for i, v in ipairs(sdata[scene or mdata.scene]) do 
		if v.char == character then
			return { main_convo = v.main_convo or false, convo = v.convo }
		end
	end

	if (scene or mdata.scene) > 1 then
		return M.get_convo(character, (scene or mdata.scene) - 1)
	end

	error("No conversation found in story data")
	return { main_convo = false, convo = {} }
end

-- next scene
function M.next_scene(scene)
	mdata.scene = scene or (mdata.scene + 1)
	return mdata.scene
end

-- get scene
function M.get_scene()
	return mdata.scene
end

return M