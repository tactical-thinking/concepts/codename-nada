# Story

This is the original story in German. Changing this document does not have any affect whatsoever. The actual game data and translations can be found in [`/main/story/`](../main/story/)

---

_door `floor_2_right_2B_2D` opens_

## Dead ends

Hiko:
> Ich frage mich, warum du noch hier bist.

Markus:
> Heute war kein leichter Tag. So etwas sieht man nicht alle Tage.

Wuulu:
> Ich frage mich von wo diese Explosion kam? Vielleicht hat man sie ja vom Schuldach aus gesehen.

Peta:
> Wenn du etwas nicht zu Ende bringst, darfst du auch nicht damit aufhören.

Sementa:
> Es gibt da so einen Jungen, den ich mag und ... wer bist du überhaupt? Verschwinde!

Hive:
> Was willst du!? Ich hatte einen schlechten Tag, also verschwinde!

Herr Schmitt:
> Was ist los? Falls du nichts brauchst, gehe ich wieder zurück an die Arbeit.

Hige:
> Wenn du mich nicht kennst, dann klopf auch nicht an meiner Tür.

## Conversation: Maks

MC:
> Hey, Hallo, ich habe da eine kleine Frage an dich kannst du mir weiterhelfen?

Maks:
> Na klar, rück raus. Was liegt dir am Herzen?

MC:
> Am Vormittag gab es doch eine Explosion, weißt du von wo die herkam?

Maks:
> Nicht ganz, aber ich kann mit Gewissheit sagen, dass sie von der anderen Seite der Schule kam. Höchstwahrscheinlich aus der 3A.

MC:
> Danke, du hast mir weitergeholfen.

Maks:
> Immer wieder gerne.

_door `floor_3_left_3A_3C` opens_

## Dead ends

Maks:
> Ob ich die Explosion gesehen habe? Nicht ganz, aber sie kam von der anderen Seite der Schule. Höchstwahrscheinlich aus der 3A.

## Monologue: ID

_examine `id`_

MC:
> Hmmm... von wem der Schülerausweis wohl sein könnte. Ich kann den Namen leider nicht erkennen. Vielleicht kann mir Herr Schmitt weiterhelfen.

## Conversation: Herr Schmitt

_door `floor_2_center` opens_

Herr Schmitt:
> Was ist los?

MC:
> Es tut mir leid, dass ich Sie stören muss, aber kennen Sie den Besitzer von diesem Schülerausweis?

Herr Schmitt:
> Ein Schülerausweis? Ich kann leider nicht erkennen, wem er gehört. Wo hast du ihn denn gefunden?

MC:
> In der 3A auf dem Boden.

Herr Schmitt:
> In der 3A sagst du. Hmmm, frag doch mal Markus. Vielleicht weiß er ja wem der Schülerausweis gehört. Er ist bestimmt hier irgendwo im 2. Stock.

MC:
> Danke, das mache ich

## Dead ends

Herr Schmitt:
> So wie ich Markus kenne, geistert er irgendwo im 2. Stock herum.

## Conversation: Markus

Markus:
> ... Heute war kein leichter Tag. So etwas sieht man nicht alle Tage.
> Hmmm, was ist das?

MC:
> Bist du Markus?

Markus:
> Der bin ich mit Leib und Seele, brauchst du etwas Bestimmtes?

MC:
> Ja, ich habe diesen Schülerausweis in der 3A gefunden und wollte wissen, ob du seinen Besitzer kennst.

Markus:
> Lass mal sehen...
> Tut mir leid, aber ich kann den Typen leider auch nicht erkennen. Du meintest doch, du hast ihn in der 3A gefunden, stimmts? Es kann gut sein, dass er einem der zwei Schüler gehört, die heute Vormittag gekämpft haben. Einer von ihnen heißt Hive und an den anderen kann ich mich nicht erinnern. Bring den Ausweis doch einfach zu ihm. Vielleicht ist es ja seiner. Ich habe ihn vor kurzem in seiner Klasse gesehen schau doch mal nach.

MC:
> Danke, du hast mir wirklich weitergeholfen.

## Dead Ends

Markus:
> Hive ist vermutlich in der 3A.

## Conversation: Hive

Hive:
> Was willst du!? Ich hatte einen schlechten Tag. Also verschwinde!

MC:
> Das hier gehört vermutlich dir.

Hive:
> Was ist das? Das könnte mein Schülerausweis sein. Danke, und entschuldige bitte meine grobe Art. Ich hatte heute nun mal keinen guten Tag.

MC:
> Warum das denn?

Hive:
> Ach, nur ein kleiner Streit unter Geschwistern. Mach dir keinen Kopf.

MC:
> Gut, wenn du das sagst.

## Dead ends

Hive:
> Wie schon gesagt, es war nur eine kleine Auseinandersetzung zwischen zwei Geschwistern. Zerbrich dir nicht weiter den Kopf darüber.

## Monologue: Owner found

MC:
> Ich sollte dem Lehrer Bescheid geben, dass ich den Besitzer des Ausweises gefunden habe.

## Conversation: Herr Schmitt

Herr Schmitt:
> Du hast den Schülerausweis seinem Besitzer gegeben? Gut, danke.

MC:
> Kein Problem.
> Ach ja, noch etwas... Kennen Sie die Geschwister von Hive?

Herr Schmitt:
> Lass mich überlegen... Soweit ich weiß, hat Hive eine kleine Schwester, die Hiko heißt. Sie geht in die 2. Klasse. Mehr weiß ich leider auch nicht.

## Conversation: Hiko

Hiko:
> Ich frage mich, warum du noch hier bist.

MC:
> Ich habe dich gesucht und muss dir eine wichtige Frage stellen... Obwohl, jetzt wo ich hier bin, bin ich mir bei der Frage auch nicht mehr sicher.

Hiko:
> Eine wichtige Frage? Jetzt werde ich nervös. Was möchtest du mich denn Fragen?

MC:
> Es geht um deinen Bruder Hive.

Hiko:
> Ach so, natürlich, schieß los.

MC:
> Hast du dich heute mit Hive geprügelt?

Hiko:
> Ich? Geprügelt? Mit meinem großen Bruder? Sei doch bitte nicht albern. Wer hat dir das erzählt?

MC:
> Ich habe nur gehört, dass sich Hive mit einem seiner Geschwister geprügelt hat, und das bist wie es aussieht nicht du.

Hiko:
> Ich weiß nicht, wovon du redest, aber ich habe meinen Bruder Hive heute noch kein einziges Mal gesehen. Ich habe aber auch noch einen älteren Bruder, Hige. Am besten du fragst ihn. Er ist vermutlich nach Hause ins Wohnheim West gegangen.
> Tür  Nummer 203, wenn du es genau wissen willst.

MC:
> Danke, du bist mir eine große Hilfe.

Hiko:
> Aber nicht doch.

## Dead ends

Hiko:
> Mein Bruder Hige ist im Wohnheim West, Tür 203, wenn du mit ihm reden möchtest.

## Conversation: Hige

_door `dorm` opens_

Hige:
> Du kennst mich nicht, also klopf auch nicht an meiner Tür.

MC:
> Du bist doch Hige, nicht wahr?

Hige:
> Woher kennst du meinen Namen?

MC:
> Ich habe mit deiner Schwester geredet und sie meinte, ich soll dich befragen.

Hige:
> Befragen!? Wegen was?

MC:
> Du bist doch für die Explosion am Vormittag verantwortlich, stimmts?  Was war der Grund dafür, dass du deinen Bruder angegriffen hast?

Hige:
> Dieser Typ hat es nicht anders verdient. Andauernd behandelt er Mädchen als wären sie wertlose Gegenstände und dieses eine mal ist er nun allemal zu weit gegangen!

MC:
> Ich verstehe. Das ist durchaus ein Grund um wütend zu werden. Wenn du nichts dagegen hast, würde ich der Sache auf den Grund gehen und deinen Bruder Hive zu dieser Sache befragen.

Hige:
> Mach doch was du willst.

## Dead ends

Hige:
> Ich dachte du wolltest unbedingt mit meinem Bruder reden, nicht das es mich kümmert.

Peta:
> Wenn man das Unmögliche ausschließt, bleibt nur mehr eine einzige Wahrheit übrig.

## Conversation: Hive

Hive:
> Du schon wieder? Kann ich dir behilflich sein?

MC:
> Ich habe da eine Frage zu heute Vormittag.

Hive:
> Eigentlich würde ich dir für diese Aufdringlichkeit auf die Schnauze hauen.
> Aber schieß los, du hast mir immerhin meinen Schülerausweis wiedergebracht.

MC:
> Hige meint, du hast ein Mädchen belästigt?

Hive:
> Was soll das!? Es stimmt zwar, dass ich, mit Ausnahme meiner kleinen Schwester, nicht viel von Mädchen halte, aber ich würde so etwas nie tun.

MC:
> Kannst du mir dann sagen, wieso er so etwas behauptet?

Hive:
> Willst du mich eigentlich auf den Arm nehmen? Woher soll ich das wissen?
> Obwohl, warte mal... Es gibt da ein Mädchen, das die ganze Zeit vor seiner Klasse steht. Ich habe da so eine Beführchtung, dass sie damit etwas zu tun haben könnte. Diese Stalkerin ist vermutlich jetzt auch auf der anderen Seite der Schule und wartet aus irgendeinem Grund vor der Klasse meines Bruders.

MC:
> Menschen gibts.
> Also gut, dann verlasse ich mich mal auf das, was du gesagt hast.
> Eine Frage hätte ich aber noch.

Hive:
> Die wäre?

MC:
> Wie kann es sein, dass du keinen Kratzer abbekommen hast?

_`hive`s sprite changes to `special`_

Hive:
> Nun ja, weißt du, ich habe die Fähigkeit mich in Feuer zu verwandeln, aber das bedeutet nicht, dass ich ohne Kratzer da rausgekommen bin.
> Den ganzen Schaden hat mein Feuerkörper einstecken müssen, deswegen stehe ich jetzt hier „ohne Kratzer“.

_`hive`s sprite changes to `special`_

MC:
> Ich verstehe, dann mach ich mich auf den Weg. Bis bald.

## Dead ends

Hive:
> Du bist immer noch hier? Das Mädchen sollte auf der anderen Seite der Schule sein.

## Conversation: Sementa

Sementa:
> ... Es gibt da so einen Jungen, den ich mag und ... wer bist du überhaupt? Verschwinde!

MC:
> Meinst du Hige? Er schaut echt gut aus und scheint ganz nett zu sein.

Sementa:
> Ja, und er ist immer so aufmerksam und großzügig. Aber was willst du?

MC:
> Ich wollte nur wissen, ob du diejenige bist, die ich suche. Weißt du eigentlich was heute Nachmittag mit Hige passiert ist?

Sementa:
> Nein. Ihm ist doch wohl nichts Schlimmes zugestoßen, oder?

MC:
> Nun ja, er hatte heute eine schwere Auseinandersetzung mit seinem Bruder und dieser hat nun Schwere Verletzungen.

Sementa:
> Was sagst du da? Hige hat seinen eigenen Bruder schwer verletzt... und das nur weil ich... Aber...

MC:
> Also steckst du hinter dem Streit zwischen den beiden Brüdern?

Sementa:
> Aber das war doch nicht meine Absicht. Ich... ich wollte doch nur, dass er mich bemerkt und mit mir Zeit verbringt.

MC:
> Und deswegen hast du Hige eine Lüge erzählt damit er sich um dich kümmert? Aufgrund deiner egoistischen Art hätte das Hive fast sein Leben gekostet...

Sementa:
> Oh mein Gott, was habe ich nur getan... Ich muss das wieder in Ordnung bringen. Bitte gehe zu Hige und frage ihn, ob er nicht mit mir reden möchte. Ich bitte dich darum.

## Dead ends

Sementa:
> Sprich mit Hige. Ich bitte dich.

## Conversation: Hige

Hige:
> Ach, du bist es. Hast du alle deine Antworten bekommen?

MC:
> Kann man so sagen, aber hör mir mal zu. Sementa, die dir das über Hive erzählt hat, hat gelogen. In Wirklichkeit ist Hive unschuldig und sie wollte nur deine Aufmerksamkeit.

Hige:
> Stimmt das auch wirklich? Du lügst mich garantiert nicht an?

MC:
> Nein, aber sie möchte sich mit dir treffen und alles in Ordnung bringen.

Hige:
> Wenn das stimmt, was du mir erzählst, dann hat diese Sementa ein echtes Problem.
> Aber gut, um alles zu klären werde ich mich wohl mit ihr treffen müssen.

## Epilogue

Hige und Sementa treffen sich und Sementa tut es unendlich Leid, dass sie Hige angelogen hat. Daraufhin treffen sich die beiden mit Hive und erklären die ganze Sache. Hive spricht daraufhin nicht mehr mit seinem Bruder. Nach 2 Monaten ist jedoch alles wieder beim Alten.

## Dead ends

Hiko:
> Ach du bist es. Danke nochmal für damals.

Markus:
> Langweilig ist doch immer am besten.

Wuulu:
> Hast du noch etwas vor?

Sementa:
> Danke nochmal für damals. Jetzt verstehe ich mich mit Hige besser denn je zuvor.

Hive:
> Hey, ist alles in Ordnung? Sag bescheid, wenn du meine Hilfe brauchst.

Herr Schmitt:
> Bleibe nicht zu lange in der Schule, ja? 

Hige:
> Was für ein ungewöhnlicher Besuch. Melde dich einfach, wenn du etwas brauchst.

Maks:
> Was für ein schöner Tag um etwas frische Luft zu schnappen.

Peta:
> Es ist nicht wichtig wer du warst. Wichtig ist nur wer du sein möchtest und dass du darauf hinarbeitest. 
> Oh, und hast du gewusst, dass ich den ersten Preis bei der weltweiten Matheolympiade gewonnen habe?
> Ich bin auch Landesmeister in Schach.
> Aber das ist noch nicht die Spitze des Eisberges.
