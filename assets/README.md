# Assets

## [`backgrounds`](backgrounds/)

### Files

All backgrounds have a height of `1080px`.

| File                                                             | Width `[px]` | Description              | Door |
|:-----------------------------------------------------------------|-------------:|:-------------------------|:----:|
| [`2B.png`](backgrounds/2B.png)                                   |       `3600` | classroom 2B             |      |
| [`3A.png`](backgrounds/3A.png)                                   |       `3600` | classroom 3A             |      |
| [`dorm.png`](backgrounds/dorm.png)                               |       `7000` | student dorms            |  ✗   |
| [`floor_2_center.png`](backgrounds/floor_2_center.png)           |       `5500` | 2nd floor center         |  ✗   |
| [`floor_2_left_2A_2C.png`](backgrounds/floor_2_left_2A_2C.png)   |       `5200` | 2nd floor left (2A, 2C)  |      |
| [`floor_2_right_2B_2D.png`](backgrounds/floor_2_right_2B_2D.png) |       `5200` | 2nd floor right (2B, 2D) |  ✗   |
| [`floor_3_left_3A_3C.png`](backgrounds/floor_3_left_3A_3C.png)   |       `5200` | 3rd floor left (3A, 3C)  |  ✗   |
| [`floor_3_right_3B_3D.png`](backgrounds/floor_3_right_3B_3D.png) |       `5200` | 3rd floor right (3B, 3D) |      |
| [`roof.png`](backgrounds/roof.png)                               |       `2000` | rooftop                  |  ✗   |
| [`street.png`](backgrounds/street.png)                           |       `3000` | street                   |      |

### Map

This map shows how backgrounds, doors and staircases are linked

```plaintext
                                                               roof.png
                                                                  ╷
                                                                  ╵
3A.png╶─╴floor_3_left_3A_3C.png╶──────────────────────╴floor_3_right_3B_3D.png
                   ╷                                              ╷
                   ╵                                              ╵
         floor_2_left_2A_2C.png╶─╴floor_2_center.png╶─╴floor_2_right_2B_2D.png╶─╴2B.png
                   ╷                                              ╷
                   │                                              │
                   └────────────────╴street.png╶──────────────────┘
                                         ╷
                                         ╵
                                      dorm.png
```

## [`characters`](characters/)

| Character   | Folder                                       | Animations                            | Location                                    | Location (`background`-file)                                     |
|:------------|:---------------------------------------------|:--------------------------------------|:--------------------------------------------|:-----------------------------------------------------------------|
| MC (Player) | [`characters/mc/`](characters/mc/)           | `default`, `examine`, `walk [00..03]` | classroom 2B                                | [`2B.png`](backgrounds/2B.png)                                   |
| Hiko        | [`characters/hiko/`](characters/hiko/)       | `default`, `wave [00..01]`            | 2nd floor below 2A window                   | [`floor_2_left_2A_2C.png`](backgrounds/floor_2_left_2A_2C.png)   |
| Hive        | [`characters/hive/`](characters/hive/)       | `default`, `special`                  | classrom 3A between rows 1 & 2              | [`3A.png`](backgrounds/3A.png)                                   |
| Maks        | [`characters/maks/`](characters/maks/)       | `fly [00..01]`                        | rooftop on the right                        | [`roof.png`](backgrounds/roof.png)                               |
| Markus      | [`characters/markus/`](characters/markus/)   | `default`                             | 2nd floor left to class 2B                  | [`floor_2_right_2B_2D.png`](backgrounds/floor_2_right_2B_2D.png) |
| Peta        | [`characters/peta/`](characters/peta/)       | `default [00..11]`                    | 3rd floor on the right                      | [`floor_3_left_3A_3C.png`](backgrounds/floor_3_left_3A_3C.png)   |
| Sementa     | [`characters/sementa/`](characters/sementa/) | `default`, `default_left`             | 3rd floor between class 3B and right window | [`floor_3_right_3B_3D.png`](backgrounds/floor_3_right_3B_3D.png) |
| Wuulu       | [`characters/wuulu/`](characters/wuulu/)     | `default`                             | classroom 2B between rows 1 & 2             | [`2B.png`](backgrounds/2B.png)                                   |

## [`doors`](doors/)

Doors consist of two files (`open.png`, `closed.png`).

| From             | To         | From (`background`-file)                                         | To (`background`-file)             | Folder                                                     |
|:-----------------|:-----------|:-----------------------------------------------------------------|:-----------------------------------|:-----------------------------------------------------------|
| Dorm             | Hige       | [`dorm.png`](backgrounds/dorm.png)                               |                                    | [`doors/dorm/`](doors/dorm/)                               |
| 2nd floor center | Schmitt    | [`floor_2_center.png`](backgrounds/floor_2_center.png)           |                                    | [`doors/floor_2_center/`](doors/floor_2_center/)           |
| 2nd floor right  | 2B         | [`floor_2_right_2B_2D.png`](backgrounds/floor_2_right_2B_2D.png) | [`2B.png`](backgrounds/2B.png)     | [`doors/floor_2_right_2B_2D/`](doors/floor_2_right_2B_2D/) |
| 3rd floor left   | 3A         | [`floor_3_left_3A_3C.png`](backgrounds/floor_3_left_3A_3C.png)   | [`3A.png`](backgrounds/3A.png)     | [`doors/floor_3_left_3A_3C/`](doors/floor_3_left_3A_3C/)   |
|                  | rooftop    |                                                                  | [`roof.png`](backgrounds/roof.png) | [`doors/roof/`](doors/roof/)                               |

## [`objects`](objects/)

| File                           | Location                     | Location (`background`-file)                                   |
|:-------------------------------|:-----------------------------|:---------------------------------------------------------------|
| [`desk.png`](objects/desk.png) | classrooms infront of player | [`2B.png`](backgrounds/2B.png), [`3A.png`](backgrounds/3A.png) |
| [`id.png`](objects/id.png)     | classroom 3A on the floor    | [`3A.png`](backgrounds/3A.png)                                 |

## [`ui`](ui/)

| File                                        |
|:--------------------------------------------|
| [`arrow_back.png`](ui/arrow_back.png)       |
| [`arrow_forward.png`](ui/arrow_forward.png) |
| [`arrow_left.png`](ui/arrow_left.png)       |
| [`arrow_up_left.png`](ui/arrow_up_left.png) |
