# Credits

## Authors

| Name           | Role                 | Contact                                                     |
|:---------------|:---------------------|:------------------------------------------------------------|
| Kürsat Salim   | Writer, Illustrator  | [salim.kursat02@gmail.com](mailto:salim.kursat02@gmail.com) |
| Lukas Kasticky | Developer            | [kasticky.cf](https://kasticky.cf)                          |

## Libraries

| Library   | Author(s)                   | Version    | License                                                       | Repository                                                                         |
|:----------|:----------------------------|:-----------|:--------------------------------------------------------------|:-----------------------------------------------------------------------------------|
| DefSave   | Brian Kramer                | `1.2.1`    | [CC0-1.0](https://creativecommons.org/publicdomain/zero/1.0/) | [github.com/subsoap/defsave](https://github.com/subsoap/defsave)                   |
| Monarch   | Björn Ritzl                 | `3.1.0`    | [MIT](https://opensource.org/licenses/mit-license.php)        | [github.com/britzl/monarch](https://github.com/britzl/monarch)                     |
| Rendercam | Ross Grams, Lukas Kasticky  | `2.0-pre3` | [Unlicense](https://unlicense.org/)                           | [github.com/lukas-kasticky/rendercam](https://github.com/lukas-kasticky/rendercam) |

# Fonts

| Font             | Author     | License                                                  |
|:-----------------|:-----------|:---------------------------------------------------------|
| Permanent Marker | Font Diner | [Apache-2.0](http://www.apache.org/licenses/LICENSE-2.0) |
| Schoolbell       | Font Diner | [Apache-2.0](http://www.apache.org/licenses/LICENSE-2.0) |

# Music

| Title            | Artist                                   | License                                                                        |
|:-----------------|:-----------------------------------------|:-------------------------------------------------------------------------------|
| Ice Road Sunrise | [Eric Matyas](http://www.soundimage.org) | [Soundimage International Public License](https://soundimage.org/sample-page/) |
| UI Quirky 12     | [Eric Matyas](http://www.soundimage.org) | [Soundimage International Public License](https://soundimage.org/sample-page/) |
